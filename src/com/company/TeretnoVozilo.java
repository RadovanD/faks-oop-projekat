package com.company;

public class TeretnoVozilo extends Vozilo {
    public float maxMasa, visina;
    TeretnoVozilo(String regBroj, Boolean obrisano, Gorivo gorivo, Servisiranje[] servisiranja, float potrosnjaGoriva, float predjenoKM, float kmDoServisiranja, float cenaServisiranja, float cenaIznajmljivanja, float brSedista, float brVrata, float maxMasa, float visina){
        super(regBroj, obrisano, gorivo, servisiranja, potrosnjaGoriva, predjenoKM, kmDoServisiranja, cenaServisiranja, cenaIznajmljivanja, brSedista, brVrata);
        this.maxMasa = maxMasa;
        this.visina = visina;
    }
}
