package com.company;

import java.util.Date;

public class Rezervacija {
    public Korisnik iznajmljivac;
    public Vozilo vozilo;
    public float cena;
    public Boolean obrisano;
    public Date pocetakRezervacije, krajRezervacije;

    Rezervacija(Korisnik iznajmljivac, Vozilo vozilo, float cena, Boolean obrisano, Date pocetakRezervacije, Date krajRezervacije){
        this.iznajmljivac = iznajmljivac;
        this.vozilo = vozilo;
        this.cena = cena;
        this.obrisano = obrisano;
        this.pocetakRezervacije = pocetakRezervacije;
        this.krajRezervacije = krajRezervacije;
    }
}
